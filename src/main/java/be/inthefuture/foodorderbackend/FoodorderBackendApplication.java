package be.inthefuture.foodorderbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class FoodorderBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodorderBackendApplication.class, args);
    }

}

package be.inthefuture.foodorderbackend.domain.settings;

import be.inthefuture.foodorderbackend.api.model.ChangeSettingCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SettingService {

    private final SettingsRepository settingsRepository;

    public List<Setting> getSettings() {
        return settingsRepository.findAll();
    }

    @Transactional
    public Setting addSetting(Setting setting) {
        checkNotNull(setting);
        return settingsRepository.save(setting);
    }

    public void changeSetting(ChangeSettingCommand changeSettingCommand) {
        checkNotNull(changeSettingCommand);
        checkNotNull(changeSettingCommand.getId());
        checkNotNull(changeSettingCommand.getKey());
        checkNotNull(changeSettingCommand.getValue());
        Optional<Setting> settingToChangeOptional = this.settingsRepository.findById(changeSettingCommand.getId());
        if (settingToChangeOptional.isPresent()) {
            Setting settingToChange = settingToChangeOptional.get();
            settingToChange.setValue(changeSettingCommand.getValue());
            this.settingsRepository.save(settingToChange);//TODO SETTING VALUE DOES NOT CHANGE
        } else {
            log.error("Setting not found: ", changeSettingCommand);
        }
    }

    public Optional<Setting> getSettingByKey(String key) {
        checkNotNull(key);
        return settingsRepository.getSettingByKey(key);
    }
}

package be.inthefuture.foodorderbackend.domain.settings;

public enum SettingType {
    BOOLEAN,
    TEXT,
    NUMBER,
    LIST
}

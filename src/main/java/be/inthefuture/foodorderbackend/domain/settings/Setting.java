package be.inthefuture.foodorderbackend.domain.settings;

import be.inthefuture.foodorderbackend.core.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "T_SETTING")
public class Setting extends AbstractEntity {

    @Column(unique = true, nullable = false)
    private String key;

    @Column(nullable = false, length = 1)
    private String value;

    @Column(nullable = false)
    private SettingType type;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SettingType getType() {
        return type;
    }

    public void setType(SettingType type) {
        this.type = type;
    }

}

package be.inthefuture.foodorderbackend.domain.catalog;

import be.inthefuture.foodorderbackend.core.AbstractEntity;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import static org.apache.commons.lang3.Validate.notBlank;

@Entity
@Table(name = "T_CATEGORY")
@Getter
public class Category extends AbstractEntity {

    @Column(unique = true, nullable = false)
    private String name;

    protected Category() {
    }

    private Category(String name) {
        notBlank(name);
        this.name = name;
    }

    public static Category of(String name) {
        return new Category(name);
    }
}

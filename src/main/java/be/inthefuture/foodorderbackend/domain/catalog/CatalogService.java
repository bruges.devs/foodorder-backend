package be.inthefuture.foodorderbackend.domain.catalog;

import be.inthefuture.foodorderbackend.api.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CatalogService {

    private final CategoryRepository categories;

    private final ProductRepository products;

    public List<Category> getCategories() {
        return categories.findAll();
    }

    public List<Product> getProductsFor(CategoryId categoryId) {
        checkNotNull(categoryId);
        Category category = categories.getOne(categoryId.getValue());
        return products.findByCategory(category);
    }

    @Transactional
    public Category createCategory(CreateCategoryRequest request) {
        checkNotNull(request);
        Category category = Category.of(request.getName());
        return categories.save(category);
    }

    @Transactional
    public void deleteCategory(CategoryId categoryId) {
        checkNotNull(categoryId);
        categories.deleteById(categoryId.getValue());
    }

    public List<Product> getProducts() {
        return products.findAll();
    }

    public Optional<Product> getProduct(ProductId productId) {
        checkNotNull(productId);
        return products.findById(productId.getValue());
    }

    @Transactional
    public Product createProduct(CreateProductRequest request) {
        checkNotNull(request);
        Category category = Optional.ofNullable(request.getCategoryId())
                .map(categoryId -> categories.getOne(request.getCategoryId().getValue()))
                .orElse(null);
        Product product = Product.of(request.getName(), request.getDescription(), category, request.getPrice());
        return products.save(product);
    }

    @Transactional
    public Product updateProduct(ProductId productId, UpdateProductRequest request) {
        checkNotNull(productId);
        checkNotNull(request);
        Category category = categories.getOne(request.getCategoryId().getValue());
        Product product = products.getOne(productId.getValue());
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        product.setCategory(category);
        product.setPrice(request.getPrice());
        return products.save(product);
    }

    @Transactional
    public void deleteProduct(ProductId productId) {
        checkNotNull(productId);
        products.deleteById(productId.getValue());
    }
}

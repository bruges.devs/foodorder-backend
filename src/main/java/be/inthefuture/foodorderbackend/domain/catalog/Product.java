package be.inthefuture.foodorderbackend.domain.catalog;

import be.inthefuture.foodorderbackend.core.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

import static org.apache.commons.lang3.Validate.*;

@Entity
@Table(name = "T_PRODUCT")
public class Product extends AbstractEntity {

    @Column(unique = true, nullable = false)
    private String name;

    private String description;

    @ManyToOne
    private Category category;

    @Column(nullable = false)
    private BigDecimal price;

    protected Product() {
    }

    private Product(String name, String description, Category category, BigDecimal price) {
        setName(name);
        setDescription(description);
        setCategory(category);
        setPrice(price);
    }

    public static Product of(String name, String description, Category category, BigDecimal price) {
        return new Product(name, description, category, price);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        notBlank(name);
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        notNull(price);
        validState(price.compareTo(BigDecimal.ZERO) > 0);
        this.price = price;
    }
}

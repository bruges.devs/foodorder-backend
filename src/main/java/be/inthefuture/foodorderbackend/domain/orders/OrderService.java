package be.inthefuture.foodorderbackend.domain.orders;

import be.inthefuture.foodorderbackend.api.model.CreateOrderRequest;
import be.inthefuture.foodorderbackend.api.model.OrderId;
import be.inthefuture.foodorderbackend.domain.catalog.Product;
import be.inthefuture.foodorderbackend.domain.catalog.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class OrderService {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    private final ProductRepository products;
    private final OrderRepository orders;

    public Optional<Order> getOrder(OrderId orderId) {
        checkNotNull(orderId);
        return orders.findById(orderId.getValue());
    }

    @Transactional
    public Order createOrder(CreateOrderRequest request, boolean isInitiallyApproved) {
        checkNotNull(request);

        Order order = new Order(isInitiallyApproved);
        order.setDateWanted(request.getDateWanted());
        order.setFirstName(request.getFirstName());
        order.setLastName(request.getLastName());
        order.setPushNotificationId(request.getPushNotificationId());

        request.getOrderItems()
                .forEach(orderItem -> {
                    Product product = products.getOne(orderItem.getProductId().getValue());
                    order.add(product, orderItem.getQuantity());
                });
        return orders.save(order);
    }

    @Transactional
    public Order approveOrder(OrderId orderId) {
        checkNotNull(orderId);
        Order order = orders.findById(orderId.getValue()).get();
        order.approve();
        order = orders.save(order);
        return order;
    }

    @Transactional
    public Order rejectOrder(OrderId orderId, String reason) {
        checkNotNull(orderId);
        Order order = orders.findById(orderId.getValue()).get();
        order.reject(reason);
        order = orders.save(order);
        return order;
    }

    public List<Order> getOrders() {
        return this.orders.findAll();
    }
}

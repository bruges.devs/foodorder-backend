package be.inthefuture.foodorderbackend.domain.orders;

import be.inthefuture.foodorderbackend.core.AbstractEntity;
import be.inthefuture.foodorderbackend.domain.catalog.Product;
import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.Validate.validState;

@Entity
@Table(name = "T_ORDER")
public class Order extends AbstractEntity {

    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderItem> items;

    private Status status;

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private LocalDateTime dateWanted;

    @Getter
    private String reason;

    @Getter
    @Setter
    private String pushNotificationId;

    public Order() {
        this.items = new ArrayList<>();
        this.status = Status.APPROVED;
    }

    public Order(boolean isInitiallyApproved) {
        this.items = new ArrayList<>();
        this.status = isInitiallyApproved ? Status.APPROVED : Status.IN_PROGRESS;
    }

    public Order add(Product product, int quantity) {
        validState(!status.equals(Status.REJECTED));

        items.stream()
                .filter(item -> item.getProduct().equals(product))
                .findFirst()
                .map(item -> item.addAmount(quantity))
                .orElseGet(() -> {
                    OrderItem item = OrderItem.of(product, quantity);
                    items.add(item);
                    return item;
                });

        return this;
    }

    public List<OrderItem> getItems() {
        return ImmutableList.copyOf(items);
    }

    public BigDecimal getTotalPrice() {
        return items.stream()
                .map(OrderItem::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public Status getStatus() {
        return status;
    }

    public void approve() {
        this.status = Status.APPROVED;
        registerEvent(OrderApproved.of(this));
    }

    public void reject(String reason) {
        this.reason = reason;
        this.status = Status.REJECTED;
    }

    public enum Status {
        IN_PROGRESS, APPROVED, REJECTED
    }
}

package be.inthefuture.foodorderbackend.domain.orders;

import be.inthefuture.foodorderbackend.core.AbstractEntity;
import be.inthefuture.foodorderbackend.domain.catalog.Product;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

import static org.apache.commons.lang3.Validate.notNull;
import static org.apache.commons.lang3.Validate.validState;

@Entity
@Table(name = "T_ORDER_ITEM")
public class OrderItem extends AbstractEntity {

    @ManyToOne
    private Product product;

    private BigDecimal productPrice;

    private int amount;

    protected OrderItem() {
    }

    private OrderItem(Product product, int amount) {
        notNull(product);
        validState(amount > 0);
        this.product = product;
        this.amount = amount;
        this.productPrice = product.getPrice();
    }

    public static OrderItem of(Product product, int amount) {
        return new OrderItem(product, amount);
    }

    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

    OrderItem addAmount(int amount) {
        this.amount += amount;
        return this;
    }

    public BigDecimal getPrice() {
        return productPrice.multiply(BigDecimal.valueOf(amount));
    }

}

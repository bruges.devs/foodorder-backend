package be.inthefuture.foodorderbackend.domain.orders;

import lombok.Getter;

import static org.apache.commons.lang3.Validate.notNull;

@Getter
public final class OrderRejected {

    private final Order order;

    private OrderRejected(Order order) {
        notNull(order);
        this.order = order;
    }

    public static OrderRejected of(Order order) {
        return new OrderRejected(order);
    }
}

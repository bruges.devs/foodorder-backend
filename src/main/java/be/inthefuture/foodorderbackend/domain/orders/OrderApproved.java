package be.inthefuture.foodorderbackend.domain.orders;

import lombok.Getter;

import static org.apache.commons.lang3.Validate.notNull;

@Getter
public final class OrderApproved {

    private final Order order;

    private OrderApproved(Order order) {
        notNull(order);
        this.order = order;
    }

    public static OrderApproved of(Order order) {
        return new OrderApproved(order);
    }
}

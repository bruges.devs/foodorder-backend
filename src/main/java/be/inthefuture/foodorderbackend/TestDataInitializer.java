package be.inthefuture.foodorderbackend;

import be.inthefuture.foodorderbackend.api.model.*;
import be.inthefuture.foodorderbackend.domain.catalog.CatalogService;
import be.inthefuture.foodorderbackend.domain.catalog.Category;
import be.inthefuture.foodorderbackend.domain.orders.OrderService;
import be.inthefuture.foodorderbackend.domain.settings.SettingService;
import be.inthefuture.foodorderbackend.domain.settings.SettingType;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
class TestDataInitializer {

    private final CatalogService catalogService;

    private final OrderService orderService;

    private final SettingService settingService;

    @EventListener
    public void init(ApplicationReadyEvent event) {
        //CATEGORIES AND PRODUCTS
        Category category = catalogService.createCategory(new CreateCategoryRequest("Snacks"));
        CategoryId categoryId = new CategoryId(category.getId());
        catalogService.createProduct(new CreateProductRequest("Frikandel", null, categoryId, new BigDecimal(1.80)));
        catalogService.createProduct(new CreateProductRequest("Mexicano", null, categoryId, new BigDecimal(2.70)));
        catalogService.createProduct(new CreateProductRequest("Viandel", null, categoryId, new BigDecimal(2.70)));
        catalogService.createProduct(new CreateProductRequest("Boulet", null, categoryId, new BigDecimal(1.90)));
        catalogService.createProduct(new CreateProductRequest("Chicken fingers (6 stuks)", null, categoryId, new BigDecimal(3.70)));
        catalogService.createProduct(new CreateProductRequest("Vuurfreter", null, categoryId, new BigDecimal(2.70)));
        catalogService.createProduct(new CreateProductRequest("Brochette", null, categoryId, new BigDecimal(2.40)));

        category = catalogService.createCategory(new CreateCategoryRequest("Frieten"));
        categoryId = new CategoryId(category.getId());
        catalogService.createProduct(new CreateProductRequest("Grote friet", null, categoryId, new BigDecimal(3.10)));
        catalogService.createProduct(new CreateProductRequest("Middel friet", null, categoryId, new BigDecimal(2.80)));
        catalogService.createProduct(new CreateProductRequest("Kleine friet", null, categoryId, new BigDecimal(2.30)));

        category = catalogService.createCategory(new CreateCategoryRequest("Broodjes"));
        categoryId = new CategoryId(category.getId());
        catalogService.createProduct(new CreateProductRequest("Smos", null, categoryId, new BigDecimal(3.50)));
        catalogService.createProduct(new CreateProductRequest("Krabsalade", null, categoryId, new BigDecimal(2.80)));
        catalogService.createProduct(new CreateProductRequest("Kip curry", null, categoryId, new BigDecimal(2.30)));

        category = catalogService.createCategory(new CreateCategoryRequest("Wraps"));
        categoryId = new CategoryId(category.getId());
        catalogService.createProduct(new CreateProductRequest("Wrap zalm", null, categoryId, new BigDecimal(4.50)));
        catalogService.createProduct(new CreateProductRequest("Wrap kip", null, categoryId, new BigDecimal(4.80)));
        catalogService.createProduct(new CreateProductRequest("Wrap veggie", null, categoryId, new BigDecimal(4.30)));

        category = catalogService.createCategory(new CreateCategoryRequest("Drank"));
        categoryId = new CategoryId(category.getId());
        catalogService.createProduct(new CreateProductRequest("Cola", null, categoryId, new BigDecimal(1.5)));
        catalogService.createProduct(new CreateProductRequest("Ice-tea", null, categoryId, new BigDecimal(1.7)));
        catalogService.createProduct(new CreateProductRequest("Fanta", null, categoryId, new BigDecimal(1.5)));

        category = catalogService.createCategory(new CreateCategoryRequest("Huisgemaakte gerechten"));
        categoryId = new CategoryId(category.getId());
        catalogService.createProduct(new CreateProductRequest("Stoofvlees met frietjes en appelmoes", null, categoryId, new BigDecimal(11.5)));
        catalogService.createProduct(new CreateProductRequest("Worst met aardappelen en appelmoes", null, categoryId, new BigDecimal(11.7)));
        catalogService.createProduct(new CreateProductRequest("Gentse waterzooi", null, categoryId, new BigDecimal(10.5)));

        catalogService.createProduct(new CreateProductRequest("Dagschotel", null, null, new BigDecimal(12)));
        catalogService.createProduct(new CreateProductRequest("Suggestie", null, null, new BigDecimal(21)));

        //ORDER
        System.out.println(orderService.createOrder(new CreateOrderRequest(Arrays.asList(
                new CreateOrderRequest.OrderItem(new ProductId(2L), 1)),
                "2a05759f-4322-4f31-bc17-64eb9758b8d4", "Anthony", "Slabinck",
                LocalDateTime.now()), true).getId());

        System.out.println(orderService.createOrder(new CreateOrderRequest(Arrays.asList(
                new CreateOrderRequest.OrderItem(new ProductId(2L), 1)),
                "2a05759f-4322-4f31-bc17-64eb9758b8d4", "Robin", "Bruneel",
                LocalDateTime.now()), true).getId());

        //SETTING
        be.inthefuture.foodorderbackend.domain.settings.Setting setting = new be.inthefuture.foodorderbackend.domain.settings.Setting();
        setting.setKey("CAN_APPROVE");
        setting.setValue("0");
        setting.setType(SettingType.BOOLEAN);
        settingService.addSetting(setting);

    }

}
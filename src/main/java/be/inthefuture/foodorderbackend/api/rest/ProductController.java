package be.inthefuture.foodorderbackend.api.rest;

import be.inthefuture.foodorderbackend.api.mapper.ProductMapper;
import be.inthefuture.foodorderbackend.api.model.CreateProductRequest;
import be.inthefuture.foodorderbackend.api.model.Product;
import be.inthefuture.foodorderbackend.api.model.ProductId;
import be.inthefuture.foodorderbackend.api.model.UpdateProductRequest;
import be.inthefuture.foodorderbackend.domain.catalog.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${rest.base-path}/products")
@RequiredArgsConstructor
public class ProductController {

    private final CatalogService catalogService;

    @GetMapping
    public List<Product> getProducts() {
        return catalogService.getProducts()
                .stream()
                .map(ProductMapper::map)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long id) {
        return catalogService.getProduct(new ProductId(id))
                .map(ProductMapper::map)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody CreateProductRequest request) {
        Product product = ProductMapper.map(catalogService.createProduct(request));
        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public Product updateProduct(@PathVariable("id") Long id, @RequestBody UpdateProductRequest request) {
        return ProductMapper.map(catalogService.updateProduct(new ProductId(id), request));
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable("id") Long id) {
        catalogService.deleteProduct(new ProductId(id));
    }
}

package be.inthefuture.foodorderbackend.api.rest;

import be.inthefuture.foodorderbackend.api.mapper.OrderCrmMapper;
import be.inthefuture.foodorderbackend.api.mapper.OrderMapper;
import be.inthefuture.foodorderbackend.api.model.CreateOrderRequest;
import be.inthefuture.foodorderbackend.api.model.Order;
import be.inthefuture.foodorderbackend.api.model.OrderCrm;
import be.inthefuture.foodorderbackend.api.model.OrderId;
import be.inthefuture.foodorderbackend.domain.orders.OrderService;
import be.inthefuture.foodorderbackend.domain.settings.SettingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${rest.base-path}/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final SettingService settingService;

    //HACK CALL TO TEST
    @GetMapping
    public ResponseEntity<List<OrderCrm>> getOrders() {
        List<OrderCrm> orders = orderService.getOrders()
                .stream().map(OrderCrmMapper::map)
                .collect(Collectors.toList());
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }
    //END HACK

    @GetMapping("/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable("id") Long id) {
        return orderService.getOrder(new OrderId(id))
                .map(OrderMapper::map)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Order> create(@RequestBody CreateOrderRequest request) {
        //TODO GET SETTING GEBASEERD OP PARTIJ
        boolean isInitiallyApproved = this.settingService.getSettingByKey("CAN_APPROVE").get().getValue().equals("1");
        Order order = OrderMapper.map(orderService.createOrder(request, isInitiallyApproved));
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @PutMapping("/{id}/approve")
    public ResponseEntity<Order> approveOrder(@PathVariable Long id) {
        Optional<Order> order = orderService.getOrder(new OrderId(id))
                .map(OrderMapper::map);

        if (order.isPresent()) {
            return new ResponseEntity(orderService.approveOrder(new OrderId(order.get().getId())), HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}/reject")
    public ResponseEntity<Order> rejectOrder(@PathVariable Long id, @RequestParam String reason) {
        Optional<Order> order = orderService.getOrder(new OrderId(id))
                .map(OrderMapper::map);

        if (order.isPresent()) {
            return new ResponseEntity(orderService.rejectOrder(new OrderId(order.get().getId()), reason), HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.NOT_FOUND);
    }
}

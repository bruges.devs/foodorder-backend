package be.inthefuture.foodorderbackend.api.rest;

import be.inthefuture.foodorderbackend.api.mapper.SettingMapper;
import be.inthefuture.foodorderbackend.api.model.ChangeSettingCommand;
import be.inthefuture.foodorderbackend.api.model.Setting;
import be.inthefuture.foodorderbackend.domain.settings.SettingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${rest.base-path}/settings")
@RequiredArgsConstructor
public class SettingController {

    private final SettingService settingService;

    @GetMapping
    public ResponseEntity<List<Setting>> getSettings() {
        List<Setting> settings = settingService.getSettings().stream()
                .map(SettingMapper::map)
                .collect(Collectors.toList());

        return new ResponseEntity<>(settings, HttpStatus.OK);
    }

    @GetMapping("/{key}")
    public ResponseEntity<Setting> getSettingByKey(@PathVariable String key){
        return this.settingService.getSettingByKey(key)
                .map(SettingMapper::map)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public void changeSetting(@RequestBody ChangeSettingCommand changeSettingCommand){
        this.settingService.changeSetting(changeSettingCommand);
    }
}

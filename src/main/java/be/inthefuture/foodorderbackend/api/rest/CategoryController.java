package be.inthefuture.foodorderbackend.api.rest;

import be.inthefuture.foodorderbackend.api.mapper.CategoryMapper;
import be.inthefuture.foodorderbackend.api.mapper.ProductMapper;
import be.inthefuture.foodorderbackend.api.model.Category;
import be.inthefuture.foodorderbackend.api.model.CategoryId;
import be.inthefuture.foodorderbackend.api.model.CreateCategoryRequest;
import be.inthefuture.foodorderbackend.api.model.Product;
import be.inthefuture.foodorderbackend.domain.catalog.CatalogService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${rest.base-path}/categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CatalogService catalogService;

    @GetMapping
    public List<Category> getCategories() {
        return catalogService.getCategories()
                .stream()
                .map(CategoryMapper::map)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}/products")
    public List<Product> getProductsFor(@PathVariable("id") Long id) {
        return catalogService.getProductsFor(new CategoryId(id))
                .stream()
                .map(ProductMapper::map)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<Category> createCategory(@RequestBody CreateCategoryRequest request) {
        Category category = CategoryMapper.map(catalogService.createCategory(request));
        return new ResponseEntity<>(category, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable("id") Long id) {
        catalogService.deleteCategory(new CategoryId(id));
    }
}

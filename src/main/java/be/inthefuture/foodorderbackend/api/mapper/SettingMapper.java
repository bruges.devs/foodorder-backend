package be.inthefuture.foodorderbackend.api.mapper;

import be.inthefuture.foodorderbackend.api.model.Setting;

public final class SettingMapper {

    public SettingMapper() {
        throw new UnsupportedOperationException("This class cannot be instantiated");
    }

    public static Setting map(be.inthefuture.foodorderbackend.domain.settings.Setting setting) {
        return Setting.builder()
                .id(setting.getId().toString())
                .key(setting.getKey())
                .value(setting.getValue())
                .type(setting.getType().name())
                .build();
    }
}

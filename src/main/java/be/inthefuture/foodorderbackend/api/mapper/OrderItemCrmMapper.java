package be.inthefuture.foodorderbackend.api.mapper;

import be.inthefuture.foodorderbackend.api.model.OrderItemCrm;
import be.inthefuture.foodorderbackend.domain.orders.OrderItem;

public class OrderItemCrmMapper {

    public static OrderItemCrm map(OrderItem orderItem) {
        return OrderItemCrm.builder()
                .amount(orderItem.getAmount())
                .price(orderItem.getPrice())
                .product(ProductMapper.map(orderItem.getProduct()))
                .build();
    }
}

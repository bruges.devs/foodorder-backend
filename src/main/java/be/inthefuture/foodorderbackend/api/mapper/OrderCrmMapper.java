package be.inthefuture.foodorderbackend.api.mapper;

import be.inthefuture.foodorderbackend.api.model.OrderCrm;
import be.inthefuture.foodorderbackend.domain.orders.Order;

import java.util.stream.Collectors;

public class OrderCrmMapper {

    public static OrderCrm map(Order order) {
        return OrderCrm.builder()
                .id(order.getId())
                .firstName(order.getFirstName())
                .lastName(order.getLastName())
                .orderItems(order.getItems().stream().map(OrderItemCrmMapper::map).collect(Collectors.toList()))
                .status(order.getStatus().toString())
                .build();
    }
}

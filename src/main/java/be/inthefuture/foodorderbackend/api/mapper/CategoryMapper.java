package be.inthefuture.foodorderbackend.api.mapper;

import be.inthefuture.foodorderbackend.api.model.Category;

public final class CategoryMapper {

    private CategoryMapper() {
        throw new UnsupportedOperationException("This class cannot be instantiated");
    }

    public static Category map(be.inthefuture.foodorderbackend.domain.catalog.Category category) {
        return Category.builder()
                .id(category.getId())
                .name(category.getName())
                .build();
    }
}
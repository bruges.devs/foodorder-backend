package be.inthefuture.foodorderbackend.api.mapper;

import be.inthefuture.foodorderbackend.api.model.Order;

public final class OrderMapper {

    private OrderMapper() {
        throw new UnsupportedOperationException("This class cannot be instantiated");
    }

    public static Order map(be.inthefuture.foodorderbackend.domain.orders.Order order) {
        return Order.builder()
                .id(order.getId())
                .status(order.getStatus().toString())
                .build();
    }
}

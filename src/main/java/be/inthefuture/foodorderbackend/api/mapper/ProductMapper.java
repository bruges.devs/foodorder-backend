package be.inthefuture.foodorderbackend.api.mapper;

import be.inthefuture.foodorderbackend.api.model.Category;
import be.inthefuture.foodorderbackend.api.model.Product;

import java.util.Optional;

public final class ProductMapper {

    private ProductMapper() {
        throw new UnsupportedOperationException("This class cannot be instantiated");
    }

    public static Product map(be.inthefuture.foodorderbackend.domain.catalog.Product product) {
        return Product.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .category(mapCategory(product.getCategory()))
                .price(product.getPrice())
                .build();
    }

    private static Category mapCategory(be.inthefuture.foodorderbackend.domain.catalog.Category category) {
        return Optional.ofNullable(category).map(CategoryMapper::map).orElse(null);
    }
}

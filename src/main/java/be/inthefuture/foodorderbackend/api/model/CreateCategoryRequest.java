package be.inthefuture.foodorderbackend.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public final class CreateCategoryRequest {

    private final String name;

    @JsonCreator
    public CreateCategoryRequest(@JsonProperty("name") String name) {
        this.name = name;
    }
}

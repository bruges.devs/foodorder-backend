package be.inthefuture.foodorderbackend.api.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Category {

    private final Long id;
    private final String name;
}

package be.inthefuture.foodorderbackend.api.model;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
public class Product {

    private final Long id;
    private final String name;
    private final String description;
    private final Category category;
    private final BigDecimal price;
}

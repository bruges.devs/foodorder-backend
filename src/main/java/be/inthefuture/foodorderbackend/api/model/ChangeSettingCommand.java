package be.inthefuture.foodorderbackend.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class ChangeSettingCommand {

    private final Long id;
    private final String key;
    private final String value;

    @JsonCreator
    public ChangeSettingCommand(@JsonProperty("id") String id,
                                @JsonProperty("key") String key,
                                @JsonProperty("value") String value) {
        this.id = new Long(id);
        this.key = key;
        this.value = value;
    }
}

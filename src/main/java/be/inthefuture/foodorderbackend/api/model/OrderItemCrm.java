package be.inthefuture.foodorderbackend.api.model;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
public class OrderItemCrm {
    private final Product product;
    private final int amount;
    private final BigDecimal price;
}

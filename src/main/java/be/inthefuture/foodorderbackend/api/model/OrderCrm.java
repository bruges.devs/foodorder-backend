package be.inthefuture.foodorderbackend.api.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class OrderCrm {

    private final Long id;
    private final List<OrderItemCrm> orderItems;
    private final String status;
    private final String firstName;
    private final String lastName;
}

package be.inthefuture.foodorderbackend.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@EqualsAndHashCode
public final class CreateOrderRequest {

    private final String pushNotificationId;
    private final String firstName;
    private final String lastName;
    private final LocalDateTime dateWanted;
    private final List<OrderItem> orderItems;

    @JsonCreator
    public CreateOrderRequest(@JsonProperty("orderItems") List<OrderItem> orderItems,
                              @JsonProperty("pushNotificationId") String pushNotificationId,
                              @JsonProperty("firstName") String firstName,
                              @JsonProperty("lastName") String lastName,
                              @JsonProperty("dateWanted") LocalDateTime dateWanted) {
        this.orderItems = orderItems;
        this.pushNotificationId = pushNotificationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateWanted = dateWanted;
    }

    @Getter
    @EqualsAndHashCode
    public static class OrderItem {

        private final ProductId productId;
        private final int quantity;

        public OrderItem(@JsonProperty("productId") ProductId productId,
                         @JsonProperty("quantity") int quantity) {
            this.productId = productId;
            this.quantity = quantity;
        }

    }
}

package be.inthefuture.foodorderbackend.api.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Setting {
    private final String id;
    private final String key;
    private final String value;
    private final String type;
}

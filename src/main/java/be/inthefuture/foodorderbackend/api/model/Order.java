package be.inthefuture.foodorderbackend.api.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Order {

    private final Long id;
    private final String status;
    private final String firstName;
    private final String lastName;
}

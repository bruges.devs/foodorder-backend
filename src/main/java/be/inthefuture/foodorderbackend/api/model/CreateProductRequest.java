package be.inthefuture.foodorderbackend.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@EqualsAndHashCode
public final class CreateProductRequest {

    private final String name;
    private final String description;
    private final CategoryId categoryId;
    private final BigDecimal price;

    @JsonCreator
    public CreateProductRequest(@JsonProperty("name") String name,
                                @JsonProperty("description") String description,
                                @JsonProperty("categoryId") CategoryId categoryId,
                                @JsonProperty("price") BigDecimal price) {
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
        this.price = price;
    }
}

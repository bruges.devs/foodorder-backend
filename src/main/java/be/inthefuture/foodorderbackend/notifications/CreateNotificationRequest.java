package be.inthefuture.foodorderbackend.notifications;

import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Map;

@Builder
@Getter
public class CreateNotificationRequest {

    private String appId;
    private List<String> includePlayerIds;
    private Map<String, Object> data;
    private Map<Language, String> contents;
}

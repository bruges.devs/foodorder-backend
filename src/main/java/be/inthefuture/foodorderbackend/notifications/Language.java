package be.inthefuture.foodorderbackend.notifications;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Language {
    DUTCH("nl"),
    ENGLISH("en"),
    FRENCH("fr");

    private final String value;

    Language(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
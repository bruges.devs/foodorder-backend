package be.inthefuture.foodorderbackend.notifications;

import be.inthefuture.foodorderbackend.domain.orders.Order;
import be.inthefuture.foodorderbackend.domain.orders.OrderApproved;
import be.inthefuture.foodorderbackend.domain.orders.OrderRejected;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
class NotificationSender {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");

    private final OneSignalClient oneSignalClient;

    @Value("${onesignal.app-id}")
    private String appId;

    @EventListener
    void on(OrderRejected event) {
        Order order = event.getOrder();

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());

        Map<Language, String> contents = new HashMap<>();
        contents.put(Language.ENGLISH, order.getReason());
        contents.put(Language.DUTCH, order.getReason());

        CreateNotificationRequest notificationRequest = CreateNotificationRequest
                .builder()
                .appId(appId)
                .includePlayerIds(Arrays.asList(order.getPushNotificationId()))
                .data(data)
                .contents(contents)
                .build();

        oneSignalClient.createNotification(notificationRequest);
    }

    @EventListener
    void on(OrderApproved event) {
        Order order = event.getOrder();

        Map<String, Object> data = new HashMap<>();
        data.put("orderId", order.getId());

        Map<Language, String> contents = new HashMap<>();
        contents.put(Language.ENGLISH, "Dag " + order.getFirstName() + ". Je bestelling zal klaar liggen om " + FORMATTER.format(order.getDateWanted()));
        contents.put(Language.DUTCH, "Dag " + order.getFirstName() + ". Je bestelling zal klaar liggen om " + FORMATTER.format(order.getDateWanted()));

        CreateNotificationRequest notificationRequest = CreateNotificationRequest
                .builder()
                .appId(appId)
                .includePlayerIds(Arrays.asList(order.getPushNotificationId()))
                .data(data)
                .contents(contents)
                .build();

        oneSignalClient.createNotification(notificationRequest);
    }
}


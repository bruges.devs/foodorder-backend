package be.inthefuture.foodorderbackend.notifications;

import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    RequestInterceptor basicAuthRequestInterceptor(@Value("${onesignal.api-key}") String apiKey) {
        return requestTemplate -> requestTemplate.header("Authorization", String.format("Basic %s", apiKey));
    }
}

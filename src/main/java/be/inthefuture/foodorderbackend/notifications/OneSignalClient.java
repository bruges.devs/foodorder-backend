package be.inthefuture.foodorderbackend.notifications;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "oneSignalClient", url = "https://onesignal.com/api/v1")
public interface OneSignalClient {

    @PostMapping("/notifications")
    void createNotification(@RequestBody CreateNotificationRequest request);
}

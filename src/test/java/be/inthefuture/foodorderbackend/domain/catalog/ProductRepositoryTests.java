package be.inthefuture.foodorderbackend.domain.catalog;

import be.inthefuture.foodorderbackend.config.JpaConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(JpaConfiguration.class)
public class ProductRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository repository;

    @Test
    public void testSave() {
        Category category = entityManager.persist(CategoryFixture.create().build());

        Product product = repository.save(ProductFixture.create().category(category).build());

        System.out.println(product.toString());

        assertThat(product.getName(), equalTo(ProductFixture.NAME));
        assertThat(product.getDescription(), equalTo(ProductFixture.DESCRIPTION));
        assertThat(product.getCategory().getName(), equalTo(CategoryFixture.NAME));
        assertThat(product.getPrice(), equalTo(ProductFixture.PRICE));
        assertThat(product.getCreatedDate(), notNullValue());
        assertThat(product.getLastModifiedDate(), notNullValue());
    }
}

package be.inthefuture.foodorderbackend.domain.catalog;

import be.inthefuture.foodorderbackend.config.JpaConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(JpaConfiguration.class)
public class CategoryRepositoryTests {

    @Autowired
    private CategoryRepository repository;

    @Test
    public void testSave() {
        repository.save(CategoryFixture.create().build());

        Category category = repository.findAll().stream()
                .filter(c -> c.getName().equals(CategoryFixture.NAME))
                .findFirst()
                .orElseThrow(AssertionError::new);

        assertThat(category.getName(), equalTo(CategoryFixture.NAME));
        assertThat(category.getCreatedDate(), notNullValue());
        assertThat(category.getLastModifiedDate(), notNullValue());
    }
}

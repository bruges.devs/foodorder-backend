package be.inthefuture.foodorderbackend.domain.catalog;

public final class CategoryFixture {

    public static final String NAME = "Snacks";

    private String name = NAME;

    private CategoryFixture() {
    }

    public static CategoryFixture create() {
        return new CategoryFixture();
    }

    public CategoryFixture name(String name) {
        this.name = name;
        return this;
    }

    public Category build() {
        return Category.of(name);
    }
}
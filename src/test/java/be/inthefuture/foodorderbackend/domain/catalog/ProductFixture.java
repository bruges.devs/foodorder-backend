package be.inthefuture.foodorderbackend.domain.catalog;

import java.math.BigDecimal;

public final class ProductFixture {

    public static final String NAME = "Frikandel";
    public static final String DESCRIPTION = null;
    public static final Category CATEGORY = CategoryFixture.create().build();
    public static final BigDecimal PRICE = new BigDecimal(1.80);

    private String name = NAME;
    private String description = DESCRIPTION;
    private Category category = CATEGORY;
    private BigDecimal price = PRICE;

    private ProductFixture() {
    }

    public static ProductFixture create() {
        return new ProductFixture();
    }

    public ProductFixture name(String name) {
        this.name = name;
        return this;
    }

    public ProductFixture description(String description) {
        this.description = description;
        return this;
    }

    public ProductFixture category(Category category) {
        this.category = category;
        return this;
    }

    public ProductFixture price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Product build() {
        return Product.of(name, description, category, price);
    }
}
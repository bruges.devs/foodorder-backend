package be.inthefuture.foodorderbackend.domain.catalog;

import be.inthefuture.foodorderbackend.api.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CatalogServiceTest {

    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private CatalogService catalogService;

    @Test
    public void testGetCategories() {
        List<Category> resultList = catalogService.getCategories();

        assertTrue(resultList.isEmpty());
    }

    @Test
    public void testGetProductsFor() {
        Category category = CategoryFixture.create().build();

        CategoryId categoryId = new CategoryId(category.getId());

        when(categoryRepository.getOne(categoryId.getValue())).thenReturn(category);

        List<Product> resultList = catalogService.getProductsFor(categoryId);

        verify(categoryRepository, times(1)).getOne(eq(categoryId.getValue()));

        verify(productRepository, times(1)).findByCategory(eq(category));

        assertTrue(resultList.isEmpty());
    }

    @Test
    public void testCreateCategory() {
        CreateCategoryRequest request = new CreateCategoryRequest(CategoryFixture.NAME);

        Category category = CategoryFixture.create().build();

        when(categoryRepository.save(category)).thenReturn(category);

        Category result = catalogService.createCategory(request);

        assertThat(result, notNullValue());
        assertThat(result.getName(), equalTo(CategoryFixture.NAME));
    }

    @Test
    public void testDeleteCategory() {
        CategoryId categoryId = new CategoryId(1L);

        catalogService.deleteCategory(categoryId);
    }

    @Test
    public void testGetProducts() {
        List<Product> resultList = catalogService.getProducts();

        assertTrue(resultList.isEmpty());
    }

    @Test
    public void testGetProduct() {
        ProductId productId = new ProductId(1L);

        Optional<Product> result = catalogService.getProduct(productId);

        assertFalse(result.isPresent());
    }

    @Test
    public void testCreateProduct() {
        CategoryId categoryId = new CategoryId(1L);
        CreateProductRequest request = new CreateProductRequest(ProductFixture.NAME, null, categoryId, ProductFixture.PRICE);

        Product product = ProductFixture.create().build();

        when(categoryRepository.getOne(categoryId.getValue())).thenReturn(CategoryFixture.create().build());

        when(productRepository.save(product)).thenReturn(product);

        Product result = catalogService.createProduct(request);

        assertThat(result, notNullValue());
        assertThat(result.getName(), equalTo(ProductFixture.NAME));
        assertThat(result.getCategory().getName(), equalTo(CategoryFixture.NAME));
        assertThat(result.getPrice(), equalTo(ProductFixture.PRICE));
    }

    @Test
    public void testUpdateProduct() {
        ProductId productId = new ProductId(1L);

        UpdateProductRequest request =
                new UpdateProductRequest(ProductFixture.NAME, null, new CategoryId(1L), ProductFixture.PRICE);

        Product product = ProductFixture.create().build();

        when(categoryRepository.getOne(request.getCategoryId().getValue())).thenReturn(CategoryFixture.create().build());

        when(productRepository.getOne(productId.getValue())).thenReturn(product);

        when(productRepository.save(product)).thenReturn(product);

        Product result = catalogService.updateProduct(productId, request);

        assertThat(result, notNullValue());
        assertThat(result.getName(), equalTo(ProductFixture.NAME));
        assertThat(result.getCategory().getName(), equalTo(CategoryFixture.NAME));
        assertThat(result.getPrice(), equalTo(ProductFixture.PRICE));
    }

    @Test
    public void testDeleteProduct() {
        ProductId productId = new ProductId(1L);

        catalogService.deleteProduct(productId);
    }
}

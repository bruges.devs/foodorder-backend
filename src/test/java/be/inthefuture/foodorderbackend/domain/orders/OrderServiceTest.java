package be.inthefuture.foodorderbackend.domain.orders;

import be.inthefuture.foodorderbackend.api.model.CreateOrderRequest;
import be.inthefuture.foodorderbackend.api.model.OrderId;
import be.inthefuture.foodorderbackend.api.model.ProductId;
import be.inthefuture.foodorderbackend.domain.catalog.Product;
import be.inthefuture.foodorderbackend.domain.catalog.ProductFixture;
import be.inthefuture.foodorderbackend.domain.catalog.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private ProductRepository productRepository;
    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void testGetOrder() {
        OrderId orderId = new OrderId(1L);

        Optional<Order> result = orderService.getOrder(orderId);

        assertFalse(result.isPresent());
    }

//    @Test
//    public void testCreateOrder() {
//        String reason = "Sorry";
//        ProductId productId = new ProductId(1L);
//        CreateOrderRequest.OrderItem orderItem = new CreateOrderRequest.OrderItem(productId, 1);
//        CreateOrderRequest request = new CreateOrderRequest(Arrays.asList(orderItem), reason, "Test","Ing");
//
//        Product product = ProductFixture.create().build();
//
//        when(productRepository.getOne(productId.getValue())).thenReturn(product);
//
//        when(orderRepository.save(any(Order.class))).thenAnswer(invocation -> invocation.getArgument(0));
//
//        Order result = orderService.createOrder(request);
//
//        assertThat(result, notNullValue());
//        assertThat(result.getItems().size(), equalTo(1));
//        assertThat(result.getReason(), equalTo(reason));
//    }
}

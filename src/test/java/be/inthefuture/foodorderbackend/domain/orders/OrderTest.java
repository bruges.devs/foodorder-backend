package be.inthefuture.foodorderbackend.domain.orders;

import be.inthefuture.foodorderbackend.domain.catalog.ProductFixture;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class OrderTest {

    @Test
    public void testGetTotalPrice() {
        Order order = new Order(false);
        order.add(ProductFixture.create().build(), 2);

        assertThat(order.getTotalPrice(), equalTo(ProductFixture.PRICE.multiply(new BigDecimal(2))));
    }
}

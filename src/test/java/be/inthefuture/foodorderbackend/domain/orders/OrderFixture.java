package be.inthefuture.foodorderbackend.domain.orders;

public final class OrderFixture {

    public static final String NAME = "Snacks";

    private String name = NAME;

    private OrderFixture() {
    }

    public static OrderFixture create() {
        return new OrderFixture();
    }

    public OrderFixture name(String name) {
        this.name = name;
        return this;
    }

    public Order build() {
        return new Order(false);
    }
}
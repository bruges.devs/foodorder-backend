package be.inthefuture.foodorderbackend.api;

import be.inthefuture.foodorderbackend.api.model.CategoryId;
import be.inthefuture.foodorderbackend.api.model.CreateCategoryRequest;
import be.inthefuture.foodorderbackend.api.rest.CategoryController;
import be.inthefuture.foodorderbackend.domain.catalog.CatalogService;
import be.inthefuture.foodorderbackend.domain.catalog.CategoryFixture;
import be.inthefuture.foodorderbackend.domain.catalog.ProductFixture;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CatalogService catalogService;

    @Test
    public void testGetCategories() throws Exception {
        given(catalogService.getCategories()).willReturn(Arrays.asList(CategoryFixture.create().build()));

        mvc.perform(get("/api/categories"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetProductsFor() throws Exception {
        CategoryId categoryId = new CategoryId(1L);

        given(catalogService.getProductsFor(categoryId)).willReturn(Arrays.asList(ProductFixture.create().build()));

        mvc.perform(get("/api/categories/{id}/products", categoryId.getValue()))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateCategory() throws Exception {
        CreateCategoryRequest request = new CreateCategoryRequest(CategoryFixture.NAME);

        given(catalogService.createCategory(request)).willReturn(CategoryFixture.create().build());

        mvc.perform(post("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated());
    }

    @Test
    public void testDeleteCategory() throws Exception {
        mvc.perform(delete("/api/categories/{id}", 1L))
                .andExpect(status().isOk());
    }
}

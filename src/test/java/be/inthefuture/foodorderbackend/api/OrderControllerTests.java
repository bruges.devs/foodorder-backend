package be.inthefuture.foodorderbackend.api;

import be.inthefuture.foodorderbackend.api.model.CreateOrderRequest;
import be.inthefuture.foodorderbackend.api.model.OrderId;
import be.inthefuture.foodorderbackend.api.model.ProductId;
import be.inthefuture.foodorderbackend.api.rest.OrderController;
import be.inthefuture.foodorderbackend.domain.orders.OrderFixture;
import be.inthefuture.foodorderbackend.domain.orders.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private OrderService orderService;

    @Test
    public void testGetOrder() throws Exception {
        OrderId orderId = new OrderId(1L);

        given(orderService.getOrder(orderId)).willReturn(Optional.of(OrderFixture.create().build()));

        mvc.perform(get("/api/orders/{id}", orderId.getValue()))
                .andExpect(status().isOk());
    }

//    @Test
//    public void testCreateOrder() throws Exception {
//        String reason = "sorry";
//        ProductId productId = new ProductId(1L);
//        CreateOrderRequest.OrderItem orderItem = new CreateOrderRequest.OrderItem(productId, 1);
//        CreateOrderRequest request = new CreateOrderRequest(Arrays.asList(orderItem), reason, "Test", "Ing");
//
//        given(orderService.createOrder(request)).willReturn(OrderFixture.create().build());
//
//        mvc.perform(post("/api/orders")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(request)))
//                .andExpect(status().isCreated());
//    }
}

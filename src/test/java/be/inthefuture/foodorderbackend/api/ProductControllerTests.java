package be.inthefuture.foodorderbackend.api;

import be.inthefuture.foodorderbackend.api.model.CategoryId;
import be.inthefuture.foodorderbackend.api.model.CreateProductRequest;
import be.inthefuture.foodorderbackend.api.model.ProductId;
import be.inthefuture.foodorderbackend.api.model.UpdateProductRequest;
import be.inthefuture.foodorderbackend.api.rest.ProductController;
import be.inthefuture.foodorderbackend.domain.catalog.CatalogService;
import be.inthefuture.foodorderbackend.domain.catalog.ProductFixture;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CatalogService catalogService;

    @Test
    public void testGetProducts() throws Exception {
        given(catalogService.getProducts()).willReturn(Arrays.asList(ProductFixture.create().build()));

        mvc.perform(get("/api/products"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetProduct() throws Exception {
        ProductId productId = new ProductId(1L);

        given(catalogService.getProduct(productId)).willReturn(Optional.of(ProductFixture.create().build()));

        mvc.perform(get("/api/products/{id}", productId.getValue()))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateProduct() throws Exception {
        CreateProductRequest request =
                new CreateProductRequest(ProductFixture.NAME, null, new CategoryId(1L), ProductFixture.PRICE);

        given(catalogService.createProduct(request)).willReturn(ProductFixture.create().build());

        mvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated());
    }

    @Test
    public void testUpdateProduct() throws Exception {
        ProductId productId = new ProductId(1L);

        UpdateProductRequest request =
                new UpdateProductRequest(ProductFixture.NAME, null, new CategoryId(1L), ProductFixture.PRICE);

        given(catalogService.updateProduct(productId, request)).willReturn(ProductFixture.create().build());

        mvc.perform(put("/api/products/{id}", productId.getValue())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteProduct() throws Exception {
        mvc.perform(delete("/api/products/{id}", 1L))
                .andExpect(status().isOk());
    }
}
